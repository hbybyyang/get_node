# get_node

在x64架构的linux上快速安装node.js
目前安装的版本是v16.15.1

## 为什么

国内安装node很难,下载太慢.

## 怎样使用

```shell
curl -L https://gitee.com/hbybyyang/get_node/raw/master/run.sh | bash
```

## TODO

- 自动识别架构(x64或ARMv7或ARMv8)
