#!/usr/bin/env bash
# curl -L https://gitee.com/hbybyyang/get_node/raw/master/run.sh | bash

set -e -o pipefail

echo '下载...'
export downloadUlr='https://lsby.coding.net/p/cdn/d/CDN/git/raw/master/node-v16.15.1-linux-x64.tar.xz?download=true'
mkdir -p /tmp/get_node
cd /tmp/get_node
curl -o node-v16.15.1-linux-x64.tar.xz `echo $downloadUlr`

echo '解压...'
tar -xf node-v16.15.1-linux-x64.tar.xz

echo '安装...'
/bin/cp -rf /tmp/get_node/node-v16.15.1-linux-x64/bin/* /usr/local/bin/
/bin/cp -rf /tmp/get_node/node-v16.15.1-linux-x64/include/* /usr/local/include/
/bin/cp -rf /tmp/get_node/node-v16.15.1-linux-x64/lib/* /usr/local/lib/
/bin/cp -rf /tmp/get_node/node-v16.15.1-linux-x64/share/* /usr/local/share/

echo '清理...'
rm -rf /tmp/get_node

echo '刷新环境...'
cd ~
source ~/.bashrc
node -v
